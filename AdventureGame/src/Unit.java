//*This is the base class for inheriting live characters in the game.*/
public abstract class Unit {
	protected String name;//name of the character
	protected int health;// health of the character 
	protected int strength;// strength of the character
	protected int max_strength;// max_strength of the character
	protected int max_health;// max_health of the character 
	protected char satiety;//satiety of the character
	

	protected String getName() {
		return this.name;
	}
	protected int getHealth() {
		return this.health;
	}
	protected int getStrength() {
		return this.strength;
	}
	protected void setName(String name) {
		this.name=name;
	}
	protected void setStrength(int strength) {
		this.strength=strength;
	}
	protected void setHealth(int health) {
		this.health=health;
	}
	void attack(Unit unit){
		unit.health-=this.strength;
	}
	
}
